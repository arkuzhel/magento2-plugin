<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Observer;

use Magento\Framework\Event\Observer;
use SevenSenders\Shipments\Helper\Data;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class OrderSaveAfter implements ObserverInterface
{
    /**
     * HelperData
     * @var Data
     */
    protected $helperData;

    /**
     * Registry
     * @var Registry
     */
    protected $registry;

    /**
     * OrderSaveAfter constructor.
     * @param Data $helperData
     * @param LoggerInterface $logger
     */
    public function __construct(
        Data $helperData,
        LoggerInterface $logger
    ) {
        $this->helperData = $helperData;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(Observer $observer)
    {
        $helper = $this->helperData;

        if (!$helper->isActive()) {
            // the plugin is not active
            return;
        }
        $this->logger->info('Triggered after save event for order');

        $order = $observer->getEvent()->getOrder();
        $sender = $helper->getClient();

        $this->logger->info('Order being saved: ' . $order->getId());

        if (!$sender->getOptions('allowed_to_track_all_shipments')
            && !$sender->getOptions('allowed_to_send_seven_senders_shipments')
        ) {
            // the user doesn't have an access
            return;
        }

        $isShippedWith7Senders = $sender->getOptions('allowed_to_send_seven_senders_shipments') && !$sender->getOptions('allowed_to_track_all_shipments');
        $order->setData('as_7senders', $isShippedWith7Senders);

        // create an order in Seven Senders
        $result = $this->helperData->createOrder($order);

        $this->logger->info('Order result from Seven Senders: ', [serialize($result)]);
    }
}