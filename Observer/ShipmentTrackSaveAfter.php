<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Shipment;
use Psr\Log\LoggerInterface;
use SevenSenders\Shipments\Helper\Data;

class ShipmentTrackSaveAfter implements ObserverInterface
{
    /**
     * HelperData
     * @var Data
     */
    protected $helperData;

    /**
     * Registry
     * @var Registry
     */
    protected $registry;

    /**
     * ShipmentModel
     * @var Shipment
     */
    protected $shipment;

    /**
     * OrderModel
     * @var Order
     */
    protected $order;

    /**
     * @var LoggerInterface
     */
    protected $logger;


    /**
     * ShipmentTrackSaveAfter constructor.
     * @param Data $helperData
     * @param Registry $registry
     * @param Order $order
     * @param Shipment $shipment
     * @param LoggerInterface $logger
     */
    public function __construct(
        Data $helperData,
        Registry $registry,
        Order $order,
        Shipment $shipment,
        LoggerInterface $logger
    ) {
        $this->helperData = $helperData;
        $this->registry = $registry;
        $this->order = $order;
        $this->shipment = $shipment;
        $this->logger = $logger;
    }

    /**
     * Event Observer
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->registry->registry('ignore_save')) {
            return;
        }

        $helper = $this->helperData;
        if (!$helper->isActive()) {
            return;
        }

        $object = $observer->getEvent()->getData('data_object');
        $orderId = $object->getOrderId();

        $order = $this->order->load($orderId);

        if (!$order->getId()) {
            return;
        }


        $sender = $helper->getClient();

        if ($sender->getOptions('allowed_to_track_all_shipments')
            && $sender->getOptions('allowed_to_send_seven_senders_shipments')
            || $sender->getOptions('allowed_to_track_all_shipments')) {
            if ($orderSeven = $this->helperData->createOrder($order)) {

                $shipment = $this->shipment->load($object->getParentId());
                if (!$shipment->getId()) {
                    return;
                }

                $shipment->setData('track_info', $object);
                $code = $this->helperData->getStoreConfig('carriers/senders7/code');
                $shipment->setData('as_7senders', $object->getData('carrier_code') == $code);
                $result = $this->helperData->createShipment($shipment, $orderSeven);

                $this->logger->info('Shipment result from Seven Senders: ', [serialize($result)]);

                if (isset($shipment)) {
                    $this->registry->unregister('current_shipment');
                    $this->registry->register('current_shipment', $shipment);
                }
            }
        }

    }
}