<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Block\Adminhtml\Shipping\View;

use SevenSenders\Shipments\Helper\Data;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Sales\Helper\Admin;
use Magento\Shipping\Block\Adminhtml\View\Form;
use Magento\Shipping\Model\CarrierFactory;

class History extends Form
{

    /**
     * Helper
     * @var Data $helperData
     */
    protected $helperData;

    /**
     * History
     * @var array $history
     */
    protected $history = [];

    /**
     * History constructor.
     * @param Context $context
     * @param Registry $registry
     * @param Admin $adminHelper
     * @param CarrierFactory $carrierFactory
     * @param Data $helperData
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Admin $adminHelper,
        CarrierFactory $carrierFactory,
        Data $helperData,
        array $data = []
    ) {
        $this->helperData = $helperData;
        parent::__construct($context, $registry, $adminHelper, $carrierFactory);
    }

    /**
     * Retrieve history by shipment
     * @return array
     */
    public function getHistory()
    {

        if (!$this->history) {
            $shipment = $this->getShipment();
            $info = $this->helperData->getSenders7Data($shipment, 'order7');

            if ($info) {
                if ($this->helperData->isActive()) {
                    $order = $this->helperData->getOrder7ByShipment($shipment);
                    if (!$order) {
                        $order = $info;
                    }
                } else {
                    $order = $info;
                }

                if ($order && isset($order['states_history']) && is_array($order['states_history'])
                    && count($order['states_history'])
                ) {
                    $this->history = $order['states_history'];
                    foreach ($this->history as $key => $item) {
                        $date = \DateTime::createFromFormat('Y-m-d\TH:i:s\+u', $item['datetime']);
                        $this->history[$key]['datetime'] = $date->format('Y-m-d H:i:s');
                    }
                }
            }
        }

        return $this->history;
    }
}