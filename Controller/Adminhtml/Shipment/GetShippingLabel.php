<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Controller\Adminhtml\Shipment;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Json\Helper\Data;
use Magento\Sales\Model\Order\ShipmentRepository;
use SevenSenders\Shipments\Helper\Data as SevenSendersData;


class GetShippingLabel extends Action
{

    /**
     * @var SevenSendersData
     */
    private $helperData;

    /**
     * @var Data
     */
    private $jsonData;

    /**
     * @var ShipmentRepository
     */
    private $shipmentRepository;

    /**
     * GetShippingLabel constructor.
     * @param Action\Context $context
     * @param SevenSendersData $helperData
     * @param Data $jsonData
     * @param ShipmentRepository $shipmentRepository
     */
    public function __construct(
        Action\Context $context,
        SevenSendersData $helperData,
        Data $jsonData,
        ShipmentRepository $shipmentRepository

    ) {
        parent::__construct($context);
        $this->helperData = $helperData;
        $this->jsonData = $jsonData;
        $this->shipmentRepository = $shipmentRepository;
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $shipmentId = $this->getRequest()->getParam('shipment_id');
        if ($shipmentId) {
            try {
                if (!$this->helperData->isActive()) {
                    $response = [
                        'status' => "error",
                        'message' => __('Seven Senders plugin is disabled.'),
                    ];
                    $response = $this->jsonData->jsonEncode($response);

                    return $this->getResponse()->representJson($response);
                }

                try {
                    $shipment = $this->shipmentRepository->get($shipmentId);
                    $url = $this->helperData->getSenders7LabelUrl($shipment);
                    if ($this->helperData->checkIfUrlExists($url)) {
                        $response = [
                            'status' => 'success',
                            'url' => $url,
                        ];
                    } else {
                        $response = [
                            'status' => "error",
                            'message' => __('Url doesn\'t exist.'),
                        ];
                    }
                } catch (NoSuchEntityException $e) {
                    $response = [
                        'status' => "error",
                        'message' => __('We can\'t initialize shipment.'),
                    ];
                }
            } catch (\Exception $e) {
                $response = [
                    'status' => "error",
                    'message' => $e->getMessage(),
                ];
            }
        }

        $response = $this->jsonData->jsonEncode($response);

        return $this->getResponse()->representJson($response);
    }
}