/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Ui/js/grid/columns/actions'
], function (Column) {
    'use strict';

    return Column.extend({
        defaults: {
            bodyTmpl: 'SevenSenders_Shipments/grid/cells/actions'
        }
    });
});
