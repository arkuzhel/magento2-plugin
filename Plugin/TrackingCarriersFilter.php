<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Plugin;

use Magento\Sales\Model\Order\Shipment;
use Magento\Shipping\Block\Adminhtml\Order\Tracking;
use SevenSenders\Shipments\Model\Carrier\AbstractCarrier;

/**
 * Class TrackingCarriersFilter
 * @package SevenSenders\Shipments\Plugin
 */
class TrackingCarriersFilter
{

    /**
     * @var \Magento\Shipping\Model\Config
     */
    protected $_shippingConfig;

    /**
     * HideCarrier constructor.
     * @param \Magento\Shipping\Model\Config $shippingConfig
     */
    public function __construct(\Magento\Shipping\Model\Config $shippingConfig)
    {
        $this->_shippingConfig = $shippingConfig;
    }

    /**
     * @param Shipment $shipment
     * @return array
     */
    protected function getCarriers(Shipment $shipment)
    {
        return $this->_shippingConfig->getActiveCarriers($shipment->getStoreId());
    }

    /**
     * Hides Carrier SevenSenders
     * @param $subject
     * @param $result
     * @return mixed
     */
    public function afterGetCarriers(Tracking $subject, $result)
    {
        // remove Seven Senders from the list
        if (isset($result['senders7'])) {
            unset($result['senders7']);
        }

        $shipment = $subject->getShipment();
        $destinationCountryId = $shipment->getShippingAddress()->getCountryId();
        $activeCarriers = $this->getCarriers($shipment);

        // filter carriers by destination country
        foreach ($result as $code => $title) {
            if (isset($activeCarriers[$code])) {
                /**
                 * @var AbstractCarrier $carrier
                 */
                $carrier = $activeCarriers[$code];
                if (!$this->checkCarrierCountrySupport($carrier, $destinationCountryId)) {
                    unset($result[$code]);
                }
            }
        }

        return $result;
    }

    /**
     * @param AbstractCarrier $carrier
     * @param $countryId
     * @return bool
     */
    protected function checkCarrierCountrySupport(AbstractCarrier $carrier, $countryId)
    {
        $speCountriesAllow = $carrier->getConfigData('sallowspecific');
        if ($speCountriesAllow && $speCountriesAllow == 1) {
            $availableCountries = [];
            if ($carrier->getConfigData('specificcountry')) {
                $availableCountries = explode(',', $carrier->getConfigData('specificcountry'));
            }
            if ($availableCountries) {
                return (in_array($countryId, $availableCountries));
            }
        }

        return true;
    }
}
