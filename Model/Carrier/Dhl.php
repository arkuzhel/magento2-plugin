<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Model\Carrier;

use Magento\Shipping\Model\Carrier\CarrierInterface;

class Dhl extends AbstractCarrier implements CarrierInterface
{
    /**
     * Carrier Code
     * @var string
     */
    protected $_code = 'dhl';

}
