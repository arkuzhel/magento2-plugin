<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;

class SevenSenders extends AbstractCarrier implements CarrierInterface
{
    /**
     * Carrier Code
     * @var string
     */
    protected $_code = 'senders7';

    /**
     * Check if carrier has shipping tracking option available
     * @return bool
     */
    public function isTrackingAvailable()
    {
        return true;
    }

    /**
     * Gets allowed methods
     * @return $this
     */
    public function getAllowedMethods()
    {
        return $this;
    }

    /**
     * Collects rates for shipping method
     * @param RateRequest $request
     * @return $this
     */
    public function collectRates(RateRequest $request)
    {
        return $this;
    }
}
