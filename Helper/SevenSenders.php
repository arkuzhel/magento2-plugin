<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

/**
 * Class SevenSenders
 */
namespace SevenSenders\Shipments\Helper;

class SevenSenders
{
    /** Constants block */
    const STRING_TYPE = 'string';
    const BOOLEAN_TYPE = 'bool';
    const INTEGER_TYPE = 'int';
    const DATE_TYPE = 'date';
    const DATE_FORMAT = 'Y-m-d\TH:i:s';

    /**
     * Url
     * @var string $_url
     */
    protected $_url = 'https://entry-point.7senders.com/api/';

    /**
     * Fields
     * @var array $_fields
     */
    protected $_fields = [
        'order' => [
            'types' => [
                'order_id' => self::STRING_TYPE,
                'order_url' => self::STRING_TYPE,
                'order_date' => self::DATE_TYPE,
                'delivered_with_seven_senders' => self::BOOLEAN_TYPE,
                'boarding_complete' => self::BOOLEAN_TYPE,
                'language' => self::STRING_TYPE,
                'promised_delivery_date' => self::DATE_TYPE
            ],
            'date_format' => self::DATE_FORMAT,
            'not_for_update' => ['delivered_with_seven_senders' => 1]
        ],
        'shipment' => [
            'types'     => [
                'order_id' => self::STRING_TYPE,
                'shipment_id' => self::STRING_TYPE,
                'reference_number' => self::STRING_TYPE,
                'tracking_code' => self::STRING_TYPE,
                'package_no' => self::INTEGER_TYPE,
                'delivered_with_seven_senders' => self::BOOLEAN_TYPE,
                'carrier' => [
                    'name' => self::STRING_TYPE,
                    'country' => self::STRING_TYPE
                ],
                'recipient_first_name' => self::STRING_TYPE,
                'recipient_last_name' => self::STRING_TYPE,
                'recipient_email' => self::STRING_TYPE,
                'recipient_address' => self::STRING_TYPE,
                'recipient_zip' => self::STRING_TYPE,
                'recipient_city' => self::STRING_TYPE,
                'recipient_country' => self::STRING_TYPE,
                'recipient_phone' => self::STRING_TYPE,
                'recipient_company_name' => self::STRING_TYPE,
                'sender_first_name' => self::STRING_TYPE,
                'sender_last_name' => self::STRING_TYPE,
                'sender_company_name' => self::STRING_TYPE,
                'sender_street' => self::STRING_TYPE,
                'sender_house_no' => self::STRING_TYPE,
                'sender_zip' => self::STRING_TYPE,
                'sender_city' => self::STRING_TYPE,
                'sender_country' => self::STRING_TYPE,
                'sender_phone' => self::STRING_TYPE,
                'sender_email' => self::STRING_TYPE,
                'cod' => self::BOOLEAN_TYPE,
                'cod_value' => self::INTEGER_TYPE,
                'return_parcel' => self::BOOLEAN_TYPE,
                'pickup_point_selected' => self::BOOLEAN_TYPE,
                'weight' => self::INTEGER_TYPE,
                'planned_pickup_datetime' => self::DATE_TYPE,
                'comment' => self::STRING_TYPE,
                'warehouse_address' => self::STRING_TYPE,
                'warehouse' => self::STRING_TYPE
            ],
            'date_format' => self::DATE_FORMAT,
            'not_for_update' => ['delivered_with_seven_senders' => 1]
        ],
    ];

    /**
     * Host
     * @var string $_host
     */
    protected $_host;

    /**
     * Token
     * @var string $_token
     */
    protected $_token;

    /**
     * Key
     * @var string $_accessKey
     */
    protected $_accessKey;

    /**
     * Client
     * @var RestClient $_client
     */
    protected $_client;

    /**
     * Options
     * @var array $_options
     */
    protected $_options = [];

    /**
     * SevenSenders constructor.
     *
     * @param string|null $accessKey
     * @param string|null $host
     */
    public function __construct($accessKey = null, $host = null)
    {
        $this->_client = new RestClient();
        $this->_setHost($host);
        if ($accessKey) {
            $this->setAccessKey($accessKey, true);
        }
    }

    /**
     * Generate token
     *
     * @return $this
     *
     * @throws \Exception
     */
    protected function _token()
    {
        if (!$this->getAccessKey() || !$this->getHost()) {
            throw new \Exception('Host and access key must be filled.');
        }
        $data = ['access_key' => $this->getAccessKey()];
        $json = json_encode($data);
        $response = $this->_client->post($json, $this->_getUrl('token'), false);
        if ($response && ($response = json_decode($response, true))
            && is_array($response) && isset($response['token'])) {
            $this->_token = $response['token'];
            $this->_options['allowed_to_track_all_shipments'] = isset($response['allowed_to_track_all_shipments'])
                ? $response['allowed_to_track_all_shipments'] : false;
            $this->_options['allowed_to_send_seven_senders_shipments'] =
                isset($response['allowed_to_send_seven_senders_shipments'])
                ? $response['allowed_to_send_seven_senders_shipments'] : false;
            $this->_client->setToken($this->getToken());
        } else {
            throw new \Exception('Authorization failed.');
            return null;
        }
        return $this;
    }

    /**
     * Generate url for REST call
     *
     * @param string $action
     * @param string|null $id
     *
     * @return string
     */
    protected function _getUrl($action, $id = null)
    {
        $url = $this->getHost() . $action;
        if ($id) {
            $url .= '/' . $id;
        }
        return trim($url, " \t\n\r\0\x0B\\");
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->_token;
    }

    /**
     * Check response for valid
     *
     * @param string|null $response
     * @param bool $graceful
     *
     * @return array|bool
     *
     * @throws \Exception
     */
    protected function _checkResponse($response, $graceful = false)
    {
        $response = json_decode($response, true);
        if (!is_array($response) && !$graceful) {
            if (!$graceful) {
                throw new \Exception('Response can not be parsed.');
            } else {
                $result = false;
            }
        } elseif (isset($response['type']) && isset($response['detail'])) {
            if (!$graceful) {
                throw new \Exception($response['detail']);
            } else {
                $result = false;
            }
        } elseif (count($response) == 0 && !$graceful) {
            if (!$graceful) {
                throw new \Exception('Not expected response.');
            } else {
                $result = false;
            }
        } else {
            $result = $response;
        }
        return $result;
    }

    /**
     * Check input variable for valid
     *
     * @param array $data
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function _checkInputs($data)
    {
        if (!is_array($data)) {
            throw new \Exception('Input var must have array type.');
        }
        if (!isset($data['order_id'])) {
            throw new \Exception('Order ID must be filled.');
        }
        return $this;
    }

    /**
     * Checks if is order unique
     *
     * @param string $id
     *
     * @return bool
     */
    protected function _isUniqueOrder($id)
    {
        return !$this->checkOrder($id);
    }

    /**
     * Checks if is order unique
     *
     * @param array $data
     *
     * @return bool
     */
    protected function _isUniqueShipment($data)
    {
        $orderId                    = isset($data['order_id'])
            ? $data['order_id']                     : null;
        $reference                  = isset($data['reference_number'])
            ? $data['reference_number']             : null;
        $tracking                   = isset($data['tracking_code'])
            ? $data['tracking_code']                : null;
        $deliveredWithSevensenders  = isset($data['delivered_with_seven_senders'])
            ? $data['delivered_with_seven_senders'] : null;
        return !$this->checkShipment($orderId, $reference, $tracking, $deliveredWithSevensenders);
    }

    /**
     * Gets Accesss Key
     * @return string
     */
    public function getAccessKey()
    {
        return $this->_accessKey;
    }

    /**
     * Sets Access Key
     * @param string $accessKey
     * @param bool $reInit
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function setAccessKey($accessKey, $reInit = false)
    {
        if (!$accessKey || empty($accessKey) || $accessKey == '') {
            throw new \Exception('Access key must be filled by not zero value.');
        }
        $this->_accessKey = (string)$accessKey;
        if ($reInit) {
            $this->_token();
        }
        return $this;
    }

    /**
     * Gets Host
     * @return string
     */
    public function getHost()
    {
        return $this->_host;
    }

    /**
     * Sets Host
     * @param string|null $host
     *
     * @return $this
     */
    public function setHost($host = null)
    {
        return $this->_setHost($host, true);
    }

    /**
     * Sets Hosts _
     * @param string|null $host
     * @param bool $strict
     *
     * @return $this
     *
     * @throws \Exception
     */
    protected function _setHost($host = null, $strict = false)
    {
        if ($strict && (!$host || empty($host) || $host == '')) {
            throw new \Exception('Host must be filled by not zero value.');
        }
        $this->_host = $host === null ? $this->_url : $host;
        return $this;
    }

    /**
     * Gets options
     * @param null $param
     *
     * @return array|mixed|null
     */
    public function getOptions($param = null)
    {
        if ($param) {
            return isset($this->_options[$param]) ? $this->_options[$param] : null;
        }
        return $this->_options;
    }

    /**
     * Retrieve order collection with filter
     *
     * @param string $id
     * @param string $deliveredWithSevenSenders
     *
     * @return array|bool
     */
    public function checkOrder($id, $deliveredWithSevenSenders = null)
    {
        $params = [
            'order_id' => $id,
            'delivered_with_seven_senders' => $deliveredWithSevenSenders ? 'true' : 'false'
        ];
        $response = $this->_client->get($this->_getUrl('orders'), $params);
        $result = $this->_checkResponse($response, true);
        return (bool)count($result);
    }

    /**
     * Retrieve order by ID
     *
     * @param string|null $id
     *
     * @return array|bool
     */
    public function getOrder($id = null)
    {
        $params = [];
        $response = $this->_client->get($this->_getUrl('orders', $id), $params);
        $result = $this->_checkResponse($response, true);
        return $result;
    }

    /**
     * Retrieve orders list
     *
     * @return array|bool
     */
    public function getOrders()
    {
        return $this->getOrder();
    }

    /**
     * Create new order
     *
     * @param array $data
     * @param boolean $graceful
     *
     * @return array|bool
     *
     * @throws \Exception
     */
    public function createOrder($data = [], $graceful = true)
    {
        $this->_checkInputs($data);
        if (!$this->_isUniqueOrder($data['order_id'])) {
            if (!$graceful) {
                throw new \Exception('Order with same ID already exist.');
            } else {
                $result = $this->getOrder($data['order_id']);
            }
        } else {
            $json = json_encode($data);
            $response = $this->_client->post($json, $this->_getUrl('orders'));
            $result = $this->_checkResponse($response);
        }
        return $result;
    }

    /**
     * Update exist order
     * @param string $id
     * @param array $data
     * @return array|bool
     * @throws \Exception
     */
    public function updateOrder($id, $data = [])
    {
        $this->_checkInputs($data);
        if ($this->_isUniqueOrder($data['order_id'])) {
            throw new \Exception('Order not found.');
        }
        $json = json_encode($data);
        $response = $this->_client->put($json, $this->_getUrl('orders', $id));
        $result = $this->_checkResponse($response);
        return $result;
    }

    /**
     * Retrieve shipment collection with filter
     *
     * @param string $orderId
     * @param string $reference
     * @param string $tracking
     * @param string $deliveredWithSevenSenders
     *
     * @return array|bool
     */
    public function checkShipment($orderId, $reference = null, $tracking = null, $deliveredWithSevenSenders = null)
    {
        $params = [
            'order_id' => $orderId,
            'reference_number' => $reference,
            'tracking_code' => $tracking,
            'delivered_with_seven_senders' => $deliveredWithSevenSenders ? 'true' : 'false',
        ];
        $response = $this->_client->get($this->_getUrl('shipments'), $params);
        $result = $this->_checkResponse($response, true);
        return (bool)count($result);
    }

    /**
     * Retrive Shipment
     * @param null $id
     *
     * @return array|bool
     */
    public function getShipment($id = null)
    {
        $params = [];
        $response = $this->_client->get($this->_getUrl('shipments', $id), $params);
        $result = $this->_checkResponse($response, true);
        return $result;
    }

    /**
     * Retrive Shipments
     * @return array|bool
     */
    public function getShipments()
    {
        return $this->getShipment();
    }

    /**
     * Create new shipment
     *
     * @param array $data
     *
     * @return array|bool
     *
     * @throws \Exception
     */
    public function createShipment($data = [])
    {
        $this->_checkInputs($data);
        $json = json_encode($data);
        $response = $this->_client->post($json, $this->_getUrl('shipments'));
        $result = $this->_checkResponse($response);
        return $result;
    }

    /**
     * Update exist shipment
     *
     * @param string $id
     * @param array $data
     *
     * @return array|bool
     *
     * @throws \Exception
     */
    public function updateShipment($id, $data = [])
    {
        $this->_checkInputs($data);
        if ($this->_isUniqueShipment($data)) {
            throw new \Exception('Shipment not found.');
        }
        $json = json_encode($data);
        $response = $this->_client->put($json, $this->_getUrl('shipments', $id));
        $result = $this->_checkResponse($response);
        return $result;
    }

    /**
     * Retrieve fields settings for entity type
     *
     * @param $entity
     *
     * @return bool|array
     */
    public function getFields($entity, $type = null)
    {
        $result = false;
        if (isset($this->_fields[$entity])) {
            $array = $this->_fields[$entity];
            switch ($type) {
                case 'create':
                    $result = array_keys($array['types']);
                    break;
                case 'update':
                    $exclude = array_keys($array['not_for_update']);
                    $result = array_diff_key(array_keys($array['types']), $exclude);
                    break;
                default:
                    $result = $array;
            }
        }
        return $result;
    }
}